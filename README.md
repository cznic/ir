# ir
Package ir implements intermediate representation of compiled programs. (Work In Progress)

Installation

    $ go get modernc.org/ir

Documentation: [godoc.org/modernc.org/ir](http://godoc.org/modernc.org/ir)
